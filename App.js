import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import GamGen from './GamGen';
import ModeSelector from './ModeSelector';
import NoteSelector from './NoteSelector';

export default function App() {

  const [note, setNote] = useState("Do")
  const [modeValue, setModeValue] = useState(0)

  return (
    <View style={styles.container}>
      <View style={styles.selectors}>
        <NoteSelector note={note} setNote={setNote} />
        <ModeSelector modeValue={modeValue} setModeValue={setModeValue} />
      </View>
      <GamGen note={note} mode={modeValue} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-around",
    backgroundColor: '#fff',
    alignItems: 'center',
    width: "100%"
  },
  selectors: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-evenly"
  }
});
