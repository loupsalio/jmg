import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function GamGen({ mode, note }) {

  function rotateLeft(arr) {
    let first = arr.shift();
    arr.push(first);
    return arr;
  }

  const notes = {
    1: "Do",
    1.5: "Do#",
    2: "Ré",
    2.5: "Ré#",
    3: "Mi",
    3.5: "Fa",
    4: "Fa#",
    4.5: "Sol",
    5: "Sol#",
    5.5: "La",
    6: "La#",
    6.5: "Si"
  }

  let splits = [1, 1, 0.5, 1, 1, 1, 0.5]
  let high = ['M', 'm', 'm', 'M', 'M', 'm', 'dim']

  for (let index = 0; index < parseInt(mode); index++) {
    splits = rotateLeft(splits);
  }

  var startPoint = parseFloat(Object.keys(notes).find(key => notes[key] === note));

  return (
    <View style={styles.container}>
      <View style={styles.notes}>
        {
          splits.map((e, i) => {
            let tmp = <View style={styles.note}>
              <Text>{notes[startPoint]}</Text>
              <Text>{high[i]}</Text>
            </View>;
            startPoint += e;
            if (startPoint > 6.5)
              startPoint = 1;
            return tmp;
          })
        }
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: "space-evenly",
    flexDirection: 'column',
    fontSize: 28,
    width: "80%"
  },
  notes: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: "row",
    fontSize: 28,
    width: "100%",
  },
  // high: {
  //   flex: 1,
  //   alignItems: 'flex-start',
  //   justifyContent: 'center',
  //   flexDirection: "row",
  //   fontSize: 28,
  //   width: "100%",
  // },
  note: {
    textAlign: 'center',
    borderWidth: 1,
    paddingHorizontal: 15
  }
});
