import React from 'react';
import { StyleSheet, Picker, View, Text } from 'react-native';

export default function NodeSelector({ note, setNote }) {

  const notes = {
    1: "Do",
    2: "Ré",
    3: "Mi",
    4: "Fa",
    5: "Sol",
    6: "La",
    7: "Si"
  }
  return (
    <View style={styles.container}>
      <Text style={styles.titre}>Gamme</Text>
      <Picker
        selectedValue={note}
        style={{ height: 50, width: 150 }}
        onValueChange={(itemValue) => setNote(itemValue)}
      >
        {
          Object.keys(notes).map((value, i) =>
            <Picker.Item key={i} label={notes[value]} value={notes[value]} />
          )
        }
      </Picker>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titre: {
    fontSize: 30
  }
});
