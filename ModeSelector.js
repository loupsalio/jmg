import React from 'react';
import { StyleSheet, Picker, View, Text } from 'react-native';

export default function ModeSelector({ modeValue, setModeValue }) {

  const modes = {
    0: "Ionien",
    1: "Dorien",
    2: "Phrygien",
    3: "Lydien",
    4: "Mixolydien",
    5: "Eolien",
    6: "Locrien"
  }
  return (
    <View style={styles.container}>
      <Text style={styles.titre}>Mode</Text>
      <Picker
        selectedValue={modeValue}
        style={{ height: 50, width: 150 }}
        onValueChange={(itemValue) => setModeValue(itemValue)}
      >
        {
          Object.keys(modes).map((value, i) =>
            <Picker.Item key={i} label={modes[value]} value={value} />
          )
        }
      </Picker>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titre: {
    fontSize: 30
  }
});
